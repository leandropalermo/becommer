package com.loja.pesca.controller.handler;

import com.loja.pesca.exception.EmailJaCadastradoException;
import com.loja.pesca.exception.ProdutoNaoEncontradoException;
import com.loja.pesca.exception.UsuarioNaoEncontradoException;
import com.loja.pesca.exception.response.ErroObjeto;
import com.loja.pesca.exception.response.ErroResponse;
import com.loja.pesca.service.impl.ProdutoServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {

    private static Logger logger = LogManager.getLogger(ControllerExceptionHandler.class);

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers,
                                                                  HttpStatus status, WebRequest request) {
        List<ErroObjeto> listaErroObjeto = getErro(ex);
        ErroResponse erroResponse = getErroResponse(ex, status, listaErroObjeto);
        logger.error("m=[handleMethodArgumentNotValid]: {}", erroResponse.toString());
        return new ResponseEntity<>(erroResponse, status);
    }

    private ErroResponse getErroResponse(MethodArgumentNotValidException ex, HttpStatus status, List<ErroObjeto> listaErroObjeto) {
        ErroResponse erroResponse = ErroResponse.builder()
                .message("Campo(s) inv\u00E1lido(s).")
                .status(status.getReasonPhrase())
                .objectName(ex.getBindingResult().getObjectName())
                .code(status.value())
                .errors(listaErroObjeto)
                .build();

        return erroResponse;
    }

    private List<ErroObjeto> getErro(MethodArgumentNotValidException ex) {
        return ex.getBindingResult().getFieldErrors().stream()
                .map(error -> new ErroObjeto(error.getDefaultMessage(), error.getField(), error.getRejectedValue()))
                .collect(Collectors.toList());
    }
}
