package com.loja.pesca.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@Builder
@NotBlank
@AllArgsConstructor
public class ListaProdutoDTO {

    private List<ProdutoDTO> produtos;

    @Override
    public String toString() {
        return "ListaProdutoDTO{" +
                "produtos=" + produtos.toString() +
                '}';
    }
}
