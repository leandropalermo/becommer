package com.loja.pesca.service;

import com.loja.pesca.dto.ListaProdutoDTO;
import com.loja.pesca.dto.ProdutoDTO;
import com.loja.pesca.exception.ProdutoNaoEncontradoException;

import java.io.Serializable;
import java.util.List;

public interface ProdutoService extends Serializable {

    ListaProdutoDTO retornarProdutos();

    ProdutoDTO pesquisarPeloCodigoProduto(String codigo) throws ProdutoNaoEncontradoException;

    void atualizarProduto(ProdutoDTO produtoDTO);

    void removerProduto(String codigo);
}
