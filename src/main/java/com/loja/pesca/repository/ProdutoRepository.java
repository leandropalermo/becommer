package com.loja.pesca.repository;

import com.loja.pesca.entity.Produto;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ProdutoRepository extends CrudRepository<Produto, Long> {

    Optional<Produto> findByCodigoProduto(String codigoProduto);
}
