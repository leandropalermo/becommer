package com.loja.pesca.converter.usuario;

import com.loja.pesca.dto.UsuarioDTO;
import com.loja.pesca.entity.Usuario;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UsuarioDTOToUsuarioConverter implements Converter<UsuarioDTO, Usuario> {

    @Override
    public Usuario convert(UsuarioDTO usuarioDTO) {
        Usuario usuario = Usuario.builder()
                .id(usuarioDTO.getCodigo())
                .nome(usuarioDTO.getNome())
                .email(usuarioDTO.getEmail())
                .password(usuarioDTO.getPassword())
                .build();

        return usuario;
    }
}
