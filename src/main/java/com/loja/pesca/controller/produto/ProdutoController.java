package com.loja.pesca.controller.produto;

import com.loja.pesca.dto.ListaProdutoDTO;
import com.loja.pesca.dto.ProdutoDTO;
import com.loja.pesca.exception.ProdutoNaoEncontradoException;
import com.loja.pesca.exception.response.ErroResponse;
import com.loja.pesca.service.ProdutoService;
import io.swagger.annotations.ApiParam;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestControllerAdvice
@RequestMapping("/produto")
public class ProdutoController {

    private static Logger logger = LogManager.getLogger(ProdutoController.class);
    private ProdutoService produtoService;

    @Autowired
    public ProdutoController(ProdutoService produtoService) {
        this.produtoService = produtoService;
    }

    @GetMapping()
    public ListaProdutoDTO retornarProdutos() {
        ListaProdutoDTO lista = produtoService.retornarProdutos();
        logger.info("m=[retornarProdutos]: {}", lista.getProdutos().toString());
        return lista;
    }

    @GetMapping("/{codigo}")
    public ProdutoDTO pesquisarPeloCodigoProduto(@PathVariable("codigo") String codigo) {
        ProdutoDTO produtoDTO = null;
        try {
            produtoDTO = produtoService.pesquisarPeloCodigoProduto(codigo);

        } catch (ProdutoNaoEncontradoException e) {
            throw e;
        }
        return produtoDTO;
    }

    @PutMapping()
    public void atualizarProduto(@ApiParam(name = "produto", value = "produto a ser enviado", required = true)  @RequestBody @Valid ProdutoDTO produto) {
        produtoService.atualizarProduto(produto);
    }

    @DeleteMapping("/{codigo}")
    public void removerProduto(@PathVariable("codigo") String codigo) {
        produtoService.removerProduto(codigo);
    }


    @ExceptionHandler(ProdutoNaoEncontradoException.class)
    private ResponseEntity<Object> handleProdutoNaoEncontradoException(ProdutoNaoEncontradoException ex) {
        ErroResponse erroResponse = ErroResponse.builder()
                .code(HttpStatus.NOT_FOUND.value())
                .message(ex.getMessage())
                .status(HttpStatus.NOT_FOUND.getReasonPhrase())
                .build();
        logger.error("m=[handleProdutoNaoEncontradoException]: {}", erroResponse.toString());
        return new ResponseEntity<>(erroResponse, HttpStatus.NOT_FOUND);
    }
}
