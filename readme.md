##Loja Produtos Para Pesca

###Detalhes Técnicos
Para que o projeto funcione com sucesso, é preciso ter instalado o Apache Maven e o Java na sua máquina.

####Iniciar
Para iniciar o projeto, primeiramente é preciso acessar a pasta principal (Pasta onde se encontra o arquivo .pom do projeto). 
Executar o comando: mvn clean install -U

Ocorrido com sucesso o processo de build do maven, agora é preciso acessar a pasta target 
existente no mesmo diretorio que se encontra o arquivo .pom.

Dentro da pasta target executar o comando: java -jar loja-pesca-1.0-SNAPSHOT.jar 

Feito isso o projeto terá sido inicializado na porta 8085.

Digitando no browse localhost:8085, irá acessar a página inicial do projeto, 
na qual é a tela de login.

Sendo o primeiro acessso, clicar no link para Cadastrar novo usuário.

