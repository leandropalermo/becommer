package com.loja.pesca.service.impl;

import com.google.common.collect.Lists;
import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;
import com.loja.pesca.dto.ListaUsuarioDTO;
import com.loja.pesca.dto.UsuarioDTO;
import com.loja.pesca.entity.Usuario;
import com.loja.pesca.exception.EmailJaCadastradoException;
import com.loja.pesca.exception.UsuarioNaoEncontradoException;
import com.loja.pesca.repository.UsuarioRepository;
import com.loja.pesca.service.UsuarioService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;

@Service
public class UsuarioServiceImpl implements UsuarioService {

    @Value("${jwt.chave-secreta}")
    private String chaveSeguranca;
    private UsuarioRepository usuarioRepository;
    private ConversionService conversionService;
    private static Logger logger = LogManager.getLogger(UsuarioServiceImpl.class);

    @Autowired
    public UsuarioServiceImpl(UsuarioRepository usuarioRepository, ConversionService conversionService) {
        this.usuarioRepository = usuarioRepository;
        this.conversionService = conversionService;
    }

    @Override
    public void adicionarNovoUsuario(UsuarioDTO usuarioDTO) {
        logger.info("m=[adicionadorNovoUsuario] - USUARIO[{}]: {}", usuarioDTO.toString());
        Optional<UsuarioDTO> resultadoBuscaUsuarioDTO = buscarUsuario(usuarioDTO.getEmail());
        if(resultadoBuscaUsuarioDTO.isPresent()) {
            throw new EmailJaCadastradoException("O email j\u00E1 est\u00E1 cadastrado.");
        }
        Usuario novoUsuario = conversionService.convert(usuarioDTO, Usuario.class);
        novoUsuario.setPassword(gerarPassword(usuarioDTO));
        usuarioRepository.save(novoUsuario);
    }

    @Override
    public Optional<UsuarioDTO> buscarUsuario(String email) {
        logger.info("m=[buscarUsuario] - email[{}]: {}", email);
        Optional<Usuario> usuario = usuarioRepository.findUsuarioByEmail(email);
        if(!usuario.isPresent()) {
            return Optional.ofNullable(null);
        }
        UsuarioDTO usuarioDTO = conversionService.convert(usuario.get(), UsuarioDTO.class);
        logger.info("m=[buscarUsuario] - retorno: {}", usuarioDTO.toString());
        return Optional.ofNullable(usuarioDTO);
    }

    @Override
    public void removerUsuario(String email) {
        logger.info("m=[removerUsuario] - email[{}]: {}", email);
        Optional<UsuarioDTO> usuarioDTO = buscarUsuario(email);
        if (!usuarioDTO.isPresent()) {
            throw new UsuarioNaoEncontradoException("N\u00E3o foi poss\u00EDvel encontrar o usu\u00E1rio.");
        }
        usuarioRepository.deleteByEmail(email);
    }

    @Override
    public ListaUsuarioDTO buscarTodosUsuario() {
        Iterable<Usuario> usuarios = usuarioRepository.findAll();
        List<Usuario> listaUsuario = Lists.newArrayList(usuarios);
        TypeDescriptor listaUsuarioTypeDescriptor = TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(Usuario.class));
        TypeDescriptor listaUsuarioDTOTypeDescriptor = TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(UsuarioDTO.class));
        List<UsuarioDTO> listaUsuarioDTO = (List<UsuarioDTO>) conversionService.convert(listaUsuario, listaUsuarioTypeDescriptor, listaUsuarioDTOTypeDescriptor);
        ListaUsuarioDTO listaResposta = new ListaUsuarioDTO(listaUsuarioDTO);

        return listaResposta;
    }

    @Override
    public boolean logarUsuario(UsuarioDTO usuarioDTO) {
        logger.info("m=[logarUsuario] - usuarioDTO[{}]: {}", usuarioDTO);
        Optional<Usuario> usuario = usuarioRepository.findUsuarioByEmail(usuarioDTO.getEmail());
        if (!usuario.isPresent()) {
            return false;
        }

        return usuario.get().getPassword().equals(gerarPassword(usuarioDTO));
    }

    private String gerarPassword(UsuarioDTO usuarioDTO) {
        HashCode hashCode = Hashing.sha256()
                .hashString(usuarioDTO.getEmail() + usuarioDTO.getPassword() + chaveSeguranca, StandardCharsets.UTF_8);
        return hashCode.toString();
    }
}
