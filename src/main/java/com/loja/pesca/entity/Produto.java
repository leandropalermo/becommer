package com.loja.pesca.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "produto", uniqueConstraints=@UniqueConstraint(columnNames="codigo_produto"))
public class Produto {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "codigo_produto")
    private String codigoProduto;

    @Column(name = "nome_produto")
    private String nomeProduto;

    @Column(name = "quantidade_produto")
    private int quantidadeProduto;

    @Override
    public String toString() {
        return "Produto{" +
                "id=" + id +
                ", codigoProduto='" + codigoProduto + '\'' +
                ", nomeProduto='" + nomeProduto + '\'' +
                ", quantidadeProduto=" + quantidadeProduto +
                '}';
    }
}
