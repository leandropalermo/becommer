package com.loja.pesca;

import com.loja.pesca.config.SwaggerConfig;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class LojaPescaApplication {
    public static void main(String[] args) {
        new SpringApplicationBuilder(LojaPescaApplication.class, SwaggerConfig.class).run(args);
    }
}
