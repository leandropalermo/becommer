package com.loja.pesca.exception.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class ErroObjeto {
    private final String message;
    private final String field;
    private final Object parameter;
}
