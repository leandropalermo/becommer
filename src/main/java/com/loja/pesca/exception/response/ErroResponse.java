package com.loja.pesca.exception.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class ErroResponse {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private final String message;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private final int code;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private final String status;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private final String objectName;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private final List<ErroObjeto> errors;

    @Override
    public String toString() {
        return "ErroResponse{" +
                "message='" + message + '\'' +
                ", code=" + code +
                ", status='" + status + '\'' +
                ", objectName='" + objectName + '\'' +
                ", errors=" + errors +
                '}';
    }
}
