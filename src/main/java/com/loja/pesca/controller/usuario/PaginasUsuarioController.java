package com.loja.pesca.controller.usuario;

import com.loja.pesca.dto.ListaUsuarioDTO;
import com.loja.pesca.dto.UsuarioDTO;
import com.loja.pesca.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import springfox.documentation.annotations.ApiIgnore;

@Controller
@ApiIgnore
public class PaginasUsuarioController {

    private UsuarioService usuarioService;

    @Autowired
    public PaginasUsuarioController(UsuarioService usuarioService) {
        this.usuarioService = usuarioService;
    }

    @RequestMapping(value = "/listaUsuario")
    public String listarUsuario(Model model) {
        ListaUsuarioDTO listaUsuarioDTO = usuarioService.buscarTodosUsuario();
        model.addAttribute("usuarios", listaUsuarioDTO.getUsuarios());
        return "listaUsuario";
    }

    @RequestMapping(value = { "/adicionarUsuario" })
    public String adicionarUsuario(Model model) {
        return "adicionarUsuario";
    }

    @RequestMapping(value = { "/gravarUsuario" }, method = RequestMethod.POST)
    public ModelAndView gravarUsuario(@ModelAttribute() UsuarioDTO usuarioDTO) {
        usuarioService.adicionarNovoUsuario(usuarioDTO);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("confirmacao-usuario");
        modelAndView.addObject("usuario", usuarioDTO);
        return modelAndView;
    }

    @RequestMapping(value = {"/logar"},  method = RequestMethod.POST)
    public ModelAndView logarUsuario(@ModelAttribute() UsuarioDTO usuario) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("listaProduto");
        modelAndView.addObject("usuario", usuario);
        if (!usuarioService.logarUsuario(usuario)) {
            modelAndView.setViewName("index");
            modelAndView.addObject("error", true);
        }

        return modelAndView;
    }

    @RequestMapping(value = { "/index" })
    public ModelAndView login(Model model) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");
        modelAndView.addObject("error", false);
        return modelAndView;
    }
}
