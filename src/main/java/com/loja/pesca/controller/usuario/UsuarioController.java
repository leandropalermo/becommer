package com.loja.pesca.controller.usuario;

import com.loja.pesca.controller.handler.ControllerExceptionHandler;
import com.loja.pesca.dto.ListaUsuarioDTO;
import com.loja.pesca.dto.UsuarioDTO;
import com.loja.pesca.exception.EmailJaCadastradoException;
import com.loja.pesca.exception.UsuarioNaoEncontradoException;
import com.loja.pesca.exception.response.ErroResponse;
import com.loja.pesca.service.UsuarioService;
import io.swagger.annotations.ApiParam;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.Optional;

@RestControllerAdvice
@RequestMapping("/usuario")
public class UsuarioController {

    private UsuarioService usuarioService;

    private static Logger logger = LogManager.getLogger(UsuarioController.class);

    public UsuarioController(UsuarioService usuarioService) {
        this.usuarioService = usuarioService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void adicionadorNovoUsuario(@ApiParam(name = "usuario", value = "cadastro de usuario", required = true)  @RequestBody @Valid UsuarioDTO usuarioDTO) {
        usuarioService.adicionarNovoUsuario(usuarioDTO);
    }


    @GetMapping("/{email}")
    public UsuarioDTO buscarUsuario(@PathVariable("email") String email) {
        Optional<UsuarioDTO> usuarioDTO = usuarioService.buscarUsuario(email);
        if (usuarioDTO.isPresent()) {
            return usuarioDTO.get();
        }
        return null;
    }

    @DeleteMapping("/{email}")
    public void removerUsuario(@PathVariable("email") String email) {
        usuarioService.removerUsuario(email);
    }

    @GetMapping
    public ListaUsuarioDTO buscarTodosUsuarios() {
        ListaUsuarioDTO listaUsuarioDTO = usuarioService.buscarTodosUsuario();
        return listaUsuarioDTO;
    }

    @ExceptionHandler(UsuarioNaoEncontradoException.class)
    private ResponseEntity<Object> handleUsuarioNaoEncontradoException(UsuarioNaoEncontradoException ex) {
        ErroResponse erroResponse = ErroResponse.builder()
                .code(HttpStatus.NOT_FOUND.value())
                .message(ex.getMessage())
                .status(HttpStatus.NOT_FOUND.getReasonPhrase())
                .build();
        logger.error("m=[handleUsuarioNaoEncontradoException]: {}", erroResponse.toString());
        return new ResponseEntity<>(erroResponse, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(EmailJaCadastradoException.class)
    private ResponseEntity<Object> handleEmailJaCadastradoException(EmailJaCadastradoException ex) {
        ErroResponse erroResponse = ErroResponse.builder()
                .code(HttpStatus.CONFLICT.value())
                .message(ex.getMessage())
                .status(HttpStatus.CONFLICT.getReasonPhrase())
                .build();
        logger.error("m=[handleEmailJaCadastradoException]: {}", erroResponse.toString());
        return new ResponseEntity<>(erroResponse, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(Exception.class)
    private ResponseEntity<Object> handleException(Exception ex) {
        ErroResponse erroResponse = ErroResponse.builder()
                .code(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .message("Erro inesperado.")
                .status(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase())
                .build();
        logger.error("m=[handleException]: {}", erroResponse.toString());
        return new ResponseEntity<>(erroResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
