package com.loja.pesca.converter.produto;

import com.loja.pesca.dto.ProdutoDTO;
import com.loja.pesca.entity.Produto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ProdutoToProdutoDTOConverter implements Converter<Produto, ProdutoDTO> {

    @Override
    public ProdutoDTO convert(Produto produto) {
        ProdutoDTO produtoDTO = ProdutoDTO.builder()
                .codigoProduto(produto.getCodigoProduto())
                .nomeProduto(produto.getNomeProduto())
                .quantidadeProduto(produto.getQuantidadeProduto())
                .build();

        return produtoDTO;
    }
}
