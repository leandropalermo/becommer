package com.loja.pesca.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

@Data
@Builder
@NotBlank
@AllArgsConstructor
public class ListaUsuarioDTO implements Serializable {

    private List<UsuarioDTO> usuarios;

    @Override
    public String toString() {
        return "ListaUsuarioDTO{" +
                "usuarios=" + usuarios +
                '}';
    }
}
