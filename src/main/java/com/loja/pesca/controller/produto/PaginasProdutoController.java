package com.loja.pesca.controller.produto;

import com.loja.pesca.dto.ListaProdutoDTO;
import com.loja.pesca.dto.ProdutoDTO;
import com.loja.pesca.service.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import springfox.documentation.annotations.ApiIgnore;

@Controller
@ApiIgnore
public class PaginasProdutoController {

    private ProdutoService produtoService;

    @Autowired
    public PaginasProdutoController(ProdutoService produtoService) {
        this.produtoService = produtoService;
    }

    @RequestMapping(value = { "/listaProduto" })
    public String listarProduto(Model model) {
        ListaProdutoDTO lista = produtoService.retornarProdutos();
        model.addAttribute("produtos", lista.getProdutos());
        return "listaProduto";
    }

    @RequestMapping(value = { "/adicionarProduto" })
    public String adicionarProduto(Model model) {
        return "adicionarProduto";
    }

    @RequestMapping(value = { "/gravarProduto" }, method = RequestMethod.POST)
    public ModelAndView gravarProduto(@ModelAttribute() ProdutoDTO produtoDTO) {
        produtoService.atualizarProduto(produtoDTO);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("confirmacao-produto");
        modelAndView.addObject("produto", produtoDTO);
        return modelAndView;
    }
}
