package com.loja.pesca.converter.usuario;

import com.loja.pesca.dto.UsuarioDTO;
import com.loja.pesca.entity.Usuario;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UsuarioToUsuarioDTOConverter implements Converter<Usuario, UsuarioDTO> {

    @Override
    public UsuarioDTO convert(Usuario usuario) {
        UsuarioDTO usuarioDTO = UsuarioDTO.builder()
                .codigo(usuario.getId())
                .email(usuario.getEmail())
                .password(usuario.getPassword())
                .nome(usuario.getNome())
                .build();

        return usuarioDTO;
    }
}
