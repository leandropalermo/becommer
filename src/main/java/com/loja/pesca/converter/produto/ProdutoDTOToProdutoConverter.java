package com.loja.pesca.converter.produto;

import com.loja.pesca.dto.ProdutoDTO;
import com.loja.pesca.entity.Produto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ProdutoDTOToProdutoConverter implements Converter<ProdutoDTO, Produto> {

    @Override
    public Produto convert(ProdutoDTO produtoDTO) {
        Produto produto = Produto.builder()
                .codigoProduto(produtoDTO.getCodigoProduto())
                .nomeProduto(produtoDTO.getNomeProduto())
                .quantidadeProduto(produtoDTO.getQuantidadeProduto())
                .build();

        return produto;
    }
}
