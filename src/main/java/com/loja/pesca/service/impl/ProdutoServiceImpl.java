package com.loja.pesca.service.impl;

import com.google.common.collect.Lists;
import com.loja.pesca.dto.ListaProdutoDTO;
import com.loja.pesca.dto.ProdutoDTO;
import com.loja.pesca.entity.Produto;
import com.loja.pesca.exception.ProdutoNaoEncontradoException;
import com.loja.pesca.repository.ProdutoRepository;
import com.loja.pesca.service.ProdutoService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProdutoServiceImpl implements ProdutoService {

    private ProdutoRepository produtoRepository;
    private ConversionService conversionService;
    private static Logger logger = LogManager.getLogger(ProdutoServiceImpl.class);

    @Autowired
    public ProdutoServiceImpl(ProdutoRepository produtoRepository, ConversionService conversionService) {
        this.produtoRepository = produtoRepository;
        this.conversionService = conversionService;
    }

    @Override
    public ListaProdutoDTO retornarProdutos() {
        Iterable<Produto> produtos = produtoRepository.findAll();
        logger.info("m=[retornarProdutos]: {}", produtos.toString());
        List<Produto> listaProduto = Lists.newArrayList(produtos);
        TypeDescriptor listaProdutoTypeDescriptor = TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(Produto.class));
        TypeDescriptor listaProdutoDTOTypeDescriptor = TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(ProdutoDTO.class));
        List<ProdutoDTO> listaProdutoDTO = (List<ProdutoDTO>) conversionService.convert(listaProduto, listaProdutoTypeDescriptor, listaProdutoDTOTypeDescriptor);

        ListaProdutoDTO listaResponse = ListaProdutoDTO.builder().produtos(listaProdutoDTO).build();
        logger.info("m=[retornarProdutos] - RETORNARNDO: {}", produtos.toString());
        return listaResponse;
    }

    @Override
    public ProdutoDTO pesquisarPeloCodigoProduto(String codigoProduto) throws ProdutoNaoEncontradoException {
        Optional<Produto> produto = produtoRepository.findByCodigoProduto(codigoProduto);
        logger.info("m=[pesquisarPeloCodigoProduto] - CODIGO_PRODUTO[{}]: {}", codigoProduto, produto.toString());
        if (!produto.isPresent()) {
            logger.info("m=[pesquisarPeloCodigoProduto] - CODIGO_PRODUTO[{}] - Produto n\u00E3o foi encontrado.", codigoProduto);
            throw new ProdutoNaoEncontradoException("Produto n\u00E3o foi encontrado.");
        }
        ProdutoDTO produtoDTO = conversionService.convert(produto.get(), ProdutoDTO.class);

        logger.info("m=[pesquisarPeloCodigoProduto] - CODIGO_PRODUTO[{}]: {} - RESPOSTA: {}", codigoProduto, produto.toString());
        return produtoDTO;
    }

    @Override
    public void atualizarProduto(ProdutoDTO produtoDTO) {
        logger.info("m=[atualizarProduto] - PRODUTO[{}]: {}", produtoDTO.toString());
        Optional<Produto> produto = produtoRepository.findByCodigoProduto(produtoDTO.getCodigoProduto());
        if (!produto.isPresent()) {
            Produto novoProduto = conversionService.convert(produtoDTO, Produto.class);
            produtoRepository.save(novoProduto);
            return;
        }
        produto.get().setNomeProduto(produtoDTO.getNomeProduto());
        produto.get().setQuantidadeProduto(produtoDTO.getQuantidadeProduto());
        produtoRepository.save(produto.get());
    }

    @Override
    public void removerProduto(String codigo) {
        logger.info("m=[removerProduto] - codigo[{}]: {}", codigo);
        Optional<Produto> produto = produtoRepository.findByCodigoProduto(codigo);
        if (!produto.isPresent()) {
            throw new ProdutoNaoEncontradoException("Produto n\u00E3o foi encontrado.");
        }
        produtoRepository.delete(produto.get());
    }
}
