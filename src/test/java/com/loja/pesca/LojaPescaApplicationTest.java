package com.loja.pesca;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LojaPescaApplicationTest {

    @Test
    public void contextLoads(){

    }

    @Test
    public void autenticacaoDoUsuarioValida() {

    }

    @Test
    public void autenticacaoDoUsuarioInvalida() {

    }

    @Test
    public void usuarioJaCadastrado() {

    }

    @Test
    public void usuarioCadastradoComSucesso() {
        
    }
}
