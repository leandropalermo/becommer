package com.loja.pesca.service;

import com.loja.pesca.dto.ListaUsuarioDTO;
import com.loja.pesca.dto.UsuarioDTO;

import java.io.Serializable;
import java.util.Optional;

public interface UsuarioService extends Serializable {

    void adicionarNovoUsuario(UsuarioDTO usuarioDTO);
    Optional<UsuarioDTO> buscarUsuario(String email);
    void removerUsuario(String email);
    ListaUsuarioDTO buscarTodosUsuario();
    boolean logarUsuario(UsuarioDTO usuarioDTO);
}
